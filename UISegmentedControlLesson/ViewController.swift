//
//  ViewController.swift
//  UISegmentedControlLesson
//
//  Created by Svyat Chaplin on 8/20/19.
//  Copyright © 2019 Svyat Chaplin. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var labelTF: UILabel!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var buttonDone: UIButton!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var labelSwitch: UILabel!
    @IBOutlet weak var switchOutlet: UISwitch!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        getData()
        
        slider.value = 50
        
        labelTF.text = String(slider.value)
        
        labelTF.font = labelTF.font.withSize(35)
        labelTF.textAlignment = .center
        labelTF.numberOfLines = 2
        
        segmentedControl.insertSegment(withTitle: "Third", at: 2, animated: true)
        slider.minimumValue = 1
        slider.maximumValue = 100
        slider.minimumTrackTintColor = .white
        slider.maximumTrackTintColor = .green
        slider.thumbTintColor = .blue
        
        labelSwitch.text = "Скрыть элементы"
        
        
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    @IBAction func onTapGesture(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func choseSegment(_ sender: UISegmentedControl) {
        labelTF.isHidden = false
        switch segmentedControl.selectedSegmentIndex {
        case 0:
            labelTF.text = "First segment"
            labelTF.textColor = .red
        case 1:
            labelTF.text = "Second segment"
            labelTF.textColor = .orange
        case 2:
            labelTF.text = "Therd segment"
            labelTF.textColor = .blue
        default:
            print("Error!")
        }
    }
    
    
    @IBAction func doneButton(_ sender: UIButton) {
        //проверяем нв наличие текста в Текстовом поле
        guard textField.text?.isEmpty == false else {return}
        //проверяем конвертируется ли текст в тип дабл (чтоб проверить ввели имя или цифры
        if let _ = Double(textField.text!){
            //в теле if в случае неправильного формата данных выводим Alert (предупреждение)
            let alert = UIAlertController(title: "Wrong Format", message: "Please enter your name", preferredStyle: .alert)
            //после того  как мы создали предупреждение, создаем новую переменную с типом UIAlertAction (действие при предупреждении)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
            // присваиваем нашему Алерту метод .addAction
            alert.addAction(okAction)
            // при помощи метода present() выводим алерт на экран
            present(alert, animated: true, completion: nil)
            print("Wrong format")
        } else {
           labelTF.text = textField.text
            textField.text = ""
        }
    }
    
    @IBAction func sliderAction(_ sender: UISlider) {
        labelTF.text = String(sender.value)
    }
    
    
    @IBAction func changeDate(_ sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.locale = Locale(identifier: "ru_RU")
        let dateValue = dateFormatter.string(from: sender.date)
        labelTF.text = dateValue
    }
    
    @IBAction func switchAction(_ sender: UISwitch) {
        // меняем настройку параметра отображения изХайден на противоположную установленной ранее при помощи знака восклицания
        segmentedControl.isHidden = !segmentedControl.isHidden
        slider.isHidden = !slider.isHidden
        labelTF.isHidden = !labelTF.isHidden
        textField.isHidden = !textField.isHidden
        datePicker.isHidden = !datePicker.isHidden
        buttonDone.isHidden = !buttonDone.isHidden
        if sender.isOn {
            labelSwitch.text = "Отобразить все элементы"
        } else {
            labelSwitch.text = "Скрыть элементы"
        }
    }
    
  
    
//    func getData() {
//        let session = URLSession.shared
//        let url = URL(string: "https://8ball.delegator.com/magic/JSON/Should%20I%20pay%20this%20employee?")!
//        session.dataTask(with: url) { data, respose, error in
//            let dict = try! JSONDecoder().decode([String: [String: String]].self, from: data!)
//            
//            
//            
//            dict["magic"]!["answer"]!
//            print()
////            JSONDecoder().decode(String.self, from: data!)
//        }.resume()
//    }

}

